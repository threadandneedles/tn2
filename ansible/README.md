# Deployment readme

## Ansible playbook

`ansible-playbook -i host.name, tn2.yml` can be run on any `host.name` if this
one has an `admin` user with no-password sudo rights, and your SSH key already
authorized. If the two last conditions are not met, you may use
`--ask-become-pass` and/or `--ask-pass` for authentication.

Don't hesitate to have a look at the `.gitlab-ci.yml` file at the root of the
project for more information on own to run the playbook.

Once this playbook has been run, the T&N website should be operational.

In the case of a migration, you will need to `rsync` the `media` folder, but
also to backup and restore the database. The following commands may come in
handy:
  * dump: `sudo -u postgres pg_dump -Fc --clean tn2_db > /tmp/tn2.dump`
  * restore: `sudo -u tn2_user pg_restore -U tn2_db_user -W -Fc --no-owner --clean -d tn2_db /tmp/tn2.dump`
