#!/bin/bash

# Required for postgres Ansible modules as both users are unprivileged
export ANSIBLE_PIPELINING=True

ansible-playbook -i ,$1 --ask-become-pass --ask-pass tn2.yml

