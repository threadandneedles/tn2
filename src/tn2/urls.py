from django.conf.urls import include
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.decorators import user_passes_test
from django.urls import reverse_lazy, re_path

from ckeditor_uploader import views as ckeditor_views
from tn2app import views as tn2_views
from tn2app.views import user as tn2_user_views

ckperms = user_passes_test(lambda user: user.has_perm('tn2app.add_article'))

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    # accounts overrides
    # These URLs are already in accounts include below but we want to change
    # the linked views of some of them without having to redefine the whole
    # thing. We intentionally don't name these URLs to avoid name clashes.
    re_path(r'^accounts/login/$',
        tn2_user_views.LoginView.as_view(
        template_name='registration/login.html'),
    ),
    re_path(r'^accounts/password/change/$',
        tn2_user_views.ChangePasswordView.as_view(
        success_url=reverse_lazy('auth_password_change_done'))
    ),
    re_path(r'^accounts/password/change/done/$',
        tn2_user_views.ChangePasswordView.as_view(done=True)
    ),
    # accounts include
    re_path(r'^accounts/', include('registration.backends.default.urls')),
    re_path(r'^ckeditor/upload/', ckperms(ckeditor_views.upload), name='ckeditor_upload'),
    re_path(r'^ckeditor/browse/', ckperms(ckeditor_views.browse), name='ckeditor_browse'),
    re_path(r'^captcha/', include('captcha.urls')),
    re_path(r'^', include('tn2app.urls')),
]

if settings.DEBUG:
    urlpatterns += [
        re_path(r'^media/(?P<path>.*)$', tn2_views.serve_media),
        re_path(
            r'^thumb/(?P<width>\d+)/(?P<height>\d+)/(?P<path>.*)$',
            tn2_views.serve_thumbnail),
    ]
