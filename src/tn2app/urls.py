from django.urls import re_path
from django.views.generic.base import RedirectView

from . import views
from .views import project as project_views, user as user_views

urlpatterns = [
    re_path(r'^$', views.Homepage.as_view(), name='homepage'),
    re_path(r'^blog/$', views.ArticleList.as_view(), name='article_list'),
    re_path(r'^blog/(?P<slug>[-%’\w]+)/$', views.ArticleDetailView.as_view(), name='article'),
    re_path(
        r'^blog/categorie/(?P<slug>[-\w]+)/$',
        views.ArticlesByCategoryList.as_view(),
        name='category'
    ),
    re_path(
        r'^blog/author/(?P<slug>[-\w]+)/$',
        views.ArticlesByAuthorList.as_view(),
        name='blog_by_author'
    ),
    re_path(r'^feed/', views.ArticleFeed(), name='blog_feed'),
    re_path(r'^projets-couture/$', project_views.ProjectList.as_view(), name='project_list'),
    re_path(
        r'^projets-couture/(?P<pk>\d+)-(?P<slug>[-\w]*)/$',
        project_views.ProjectDetails.as_view(),
        name='project_details'
    ),
    re_path(
        r'^projets-couture/(?P<pk>\d+)-(?P<slug>[-\w]*)/edit/$',
        project_views.ProjectEdit.as_view(),
        name='project_edit'
    ),
    re_path(
        r'^projets-couture/(?P<pk>\d+)-(?P<slug>[-\w]*)/jaime/$',
        project_views.ProjectLike.as_view(),
        name='project_like'
    ),
    re_path(
        r'^projets-couture/(?P<pk>\d+)-(?P<slug>[-\w]*)/favoris/$',
        project_views.ProjectFavorite.as_view(),
        name='project_favorite'
    ),
    re_path(
        r'^projets-couture/(?P<pk>\d+)-(?P<slug>[-\w]*)/a-la-une/$',
        project_views.ProjectFeature.as_view(),
        name='project_feature'
    ),
    re_path(r'^groupes/$', views.DiscussionGroupListView.as_view(), name='discussion_groups'),
    # the "%" and "’" characters aren't part of django's slug, but it's part of our "legacy" slugs.
    re_path(
        r'^groupes/(?P<group_slug>[-%’\w]+)/home/$',
        views.DiscussionGroupDetailView.as_view(),
        name='discussion_group'
    ),
    re_path(r'^groupes/(?P<group_slug>[-%’\w]+)/forum/topic/add/$', views.DiscussionAdd.as_view(), name='discussion_add'),
    re_path(
        r'^groupes/(?P<group_slug>[-%’\w]+)/forum/topic/(?P<discussion_slug>[-%’\w]+)/$',
        views.DiscussionDetailView.as_view(),
        name='discussion'
    ),
    re_path(
        r'^groupes/(?P<group_slug>[-%’\w]+)/forum/topic/(?P<discussion_slug>[-%’\w]+)/edit/$',
        views.DiscussionEdit.as_view(),
        name='discussion_edit'
    ),
    re_path(
        r'^groupes/(?P<group_slug>[-%’\w]+)/forum/topic/(?P<discussion_slug>[-%’\w]+)/bookmark/$',
        views.DiscussionBookmark.as_view(),
        name='discussion_bookmark'
    ),
    re_path(r'^comments/(?P<model>\w+)/(?P<model_pk>\d+)/add/$', views.CommentAdd.as_view(), name='comment_add'),
    re_path(r'^comments/(?P<model>\w+)/(?P<comment_pk>\d+)/edit/$', views.CommentEdit.as_view(), name='comment_edit'),
    re_path(r'^comments/(?P<model>\w+)/(?P<comment_pk>\d+)/delete/$', views.CommentDelete.as_view(), name='comment_delete'),
    re_path(
        r'^membres/(?P<username>[-\w.]+)/profil/$',
        user_views.UserProfileView.as_view(),
        name='user_profile'
    ),
    re_path(
        r'^membres/(?P<username>[-\w.]+)/$',
        RedirectView.as_view(pattern_name='user_profile'),
        name='user_profile_redirect'
    ),
    re_path(
        r'^membres/(?P<username>[-\w.]+)/profil/edit/$',
        user_views.UserProfileEdit.as_view(),
        name='user_profile_edit'
    ),
    re_path(
        r'^membres/(?P<username>[-\w.]+)/projets-couture/nouveau-projet/$',
        project_views.ProjectCreate.as_view(),
        name='project_create'
    ),
    re_path(
        r'^membres/(?P<username>[-\w.]+)/favoris/$',
        user_views.UserFavoritesView.as_view(),
        name='user_favorites'
    ),
    re_path(
        r'^membres/(?P<username>[-\w.]+)/contacter/$',
        user_views.UserSendMessageView.as_view(),
        name='user_sendmessage'
    ),
    re_path(
        r'^notifications/$',
        user_views.UserNotificationsView.as_view(),
        name='user_notifications',
    ),
    re_path(
        r'^discussions/$',
        user_views.UserDiscussionsView.as_view(),
        name='user_discussions',
    ),
    re_path(
        r'^messages/$',
        user_views.UserMessagesView.as_view(),
        name='user_messages',
    ),
    re_path(
        r'^search/$',
        views.CompoundSearchView.as_view(),
        name='search',
    ),
    re_path(
        r'^search/articles/$',
        views.ArticleSearchView.as_view(),
        name='search_article',
    ),
    re_path(
        r'^search/projets/$',
        views.ProjectSearchView.as_view(),
        name='search_project',
    ),
    re_path(
        r'^search/discussions/$',
        views.DiscussionSearchView.as_view(),
        name='search_discussion',
    ),

    re_path(r'^contact/$', views.ContactView.as_view(), name='page_contact'),

    re_path(r'^ajax/patterns/(?P<creator_id>\d+)/$', views.PatternListJSON.as_view(), name='ajax_patterns'),
]

PAGES = [
    'a-propos', 'cgu', 'foire-aux-questions', 'presse', 'sponsors',
    'envoyez-nous-vos-articles', 'soutenir', 'transition-fr-org',
]

urlpatterns += [
    re_path(r'^{}/$'.format(p), views.PageView.as_view(pagename=p), name='page_{}'.format(p))
    for p in PAGES
]

