# Generated by Django 2.2.24 on 2021-11-28 21:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('tn2app', '0035_discussion_bookmarked_by'),
    ]

    operations = [
        migrations.AlterField(
            model_name='notification',
            name='type',
            field=models.PositiveSmallIntegerField(choices=[(1, 'Réponse dans une discussion'), (2, 'Vote pour un projet'), (3, 'Message privé'), (4, 'Commentaire sur un projet')], db_index=True),
        ),
    ]
