var tn2_project_details = {
    setup: function() {
        tn2_project_details.bind_image_previews();
        tn2_project_details.bind_image_prev_next();
    },

    bind_image_previews: function() {
        var preview_images = document.querySelectorAll('img.project-image-preview');
        for (var i=0; i<preview_images.length; i++) {
            preview_images[i].addEventListener('click', function() {
                var full_url = this.getAttribute('data-full-url');
                var image_id = this.getAttribute('data-image-id');
                var fullsize_img = document.querySelector('#full-size-image');
                fullsize_img.setAttribute('src', full_url);
                fullsize_img.setAttribute('image-id', image_id);
            });
        }
    },

    bind_image_prev_next: function() {
        var preview_images = document.querySelectorAll('img.project-image-preview');
        if (preview_images.length > 0) {
            document.querySelector('#full-size-image').addEventListener('click', function(e) {
                if (e.x > this.x + this.width / 2) {
                    // next image
                    tn2_project_details.image_prev_next("next");
                } else {
                    // previous image
                    tn2_project_details.image_prev_next("prev");
                }
            });
            document.querySelector('#full-size-image-prev').addEventListener('click', function(e) {
                tn2_project_details.image_prev_next("prev");
            });
            document.querySelector('#full-size-image-next').addEventListener('click', function(e) {
                tn2_project_details.image_prev_next("next");
            });
        }
    },

    image_prev_next: function(prev_or_next) {
        var full_size_image = document.querySelector('#full-size-image');
        var preview_images = document.querySelectorAll('img.project-image-preview');
        var image_id = Number(full_size_image.getAttribute('image-id'));
        var target_id;
        if (prev_or_next == "next") {
            // next image
            target_id = (image_id == preview_images.length ? 1 : image_id + 1);
        } else {
            // previous image
            target_id = (image_id == 1 ? preview_images.length : image_id - 1);
        }
        var target_url = document.querySelector("img.project-image-preview[data-image-id='" + target_id + "']").getAttribute('data-full-url');
        full_size_image.setAttribute('src', target_url);
        full_size_image.setAttribute('image-id', target_id);
    },
}

document.addEventListener('DOMContentLoaded', tn2_project_details.setup);
